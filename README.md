# vue-typescript

> A Vue.js project

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://toilal.github.io/vue-webpack-template/).

This temnplate builds on the fine work done by Toilal in creating a vue typescript seed project based on the vue cli webpacked template. This template adds pug support as well as cleans up the folder structure and adds prettier linting configm plus adds vuetify as default style framework.
