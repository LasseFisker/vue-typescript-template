// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import Vuetify from "vuetify";
import App from "./components/app/app.vue";
import router from "./router";

Vue.config.productionTip = false;
Vue.use(Vuetify);

// tslint:disable-next-line:no-unused-expression
new Vue({
  router,
  el: "#app",
  template: "<App/>",
  components: { App }
});
